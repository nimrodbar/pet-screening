var breed = 'none';
var breedII = 'none';
var breedIIEnabled = false;
var age, isMale, isFemale;
var version = "1.0";

function updateUrlHash() {
    var hashParamsArr = [];

    if (typeof breed !== 'undefined') {
    	hashParamsArr.push('breed=' + breed);
    }
    if (typeof age !== 'undefined') {
    	hashParamsArr.push('age=' + age);	
    }
    if ((typeof isMale !== 'undefined') || (typeof isFemale !== 'undefined')) {
    	var gender = isMale ? 'male' : 'female';
    	hashParamsArr.push('gender=' + gender);		
    }
    $.each($("#signs input[type='checkbox']:checked"), function() {
    	hashParamsArr.push($(this).prop('name'));
	});

    window.location.replace(('' + window.location).split('#')[0] + '#' + hashParamsArr.join('&'));
    $('.copy').attr('data-clipboard-text', window.location);
    var mailto = $.tmpl(locales[lang]['share-mailto'], {'url': encodeURIComponent(window.location)}).text();
    $('.mailto').attr('href', 'mailto:?' + mailto);
    var whatsapp = $.tmpl(locales[lang]['share-whatsapp'], {'url': encodeURIComponent(window.location)}).text();
	$('.whatsapp').attr('href', 'https://api.whatsapp.com/send?text=' + whatsapp);
};

function updateFromHashParams() {
    var hashParamsArray = window.location.hash.substring(1).split('&');
   
    for (var i=0; i<hashParamsArray.length; i++) {
    	var param = decodeURIComponent(hashParamsArray[i]);
    	if (param.indexOf('=') > -1) {
    		key = param.split('=')[0];
    		val = param.split('=')[1];
    		$('#' + key + '-select').dropdown('set selected', val);
    	} else {
    		$('input[name="' + param + '"]').parent().checkbox('check');
    	}
     }

     if ((hashParamsArray.length > 0) && (hashParamsArray[0].length > 0)) {
     	onCheck('hash');
     }
};


function onBreedSelected(value, text, $selectedItem) {
	 hideResults();
	 breed = value;
	 setAnalyzeState();
}
function onBreedIISelected(value, text, $selectedItem) {
	 hideResults();
	 breedII = value;
	 setAnalyzeState();
}
function onAgeSelected(value, text, $selectedItem) {
	 hideResults();
	 if (typeof text !== 'undefined') {
	 	age = parseInt(text, 10);
	 } else {
	 	age = undefined;
	 }
	 setAnalyzeState();
}
function onGenderSelected(value, text, $selectedItem) {
	hideResults();
	var gender = value.toLowerCase();
	if (gender == 'male') {
	 	isMale = true;
	 	isFemale = false;
	} else if (gender == 'female') {
	 	isFemale = true;
	 	isMale = false;
	} else {
		isMale = undefined;
		isFemale = undefined;
	}
}
function onSymptomChecked() {
	hideResults();
	setAnalyzeState();
}
function onSymptomUnchecked() {
	hideResults();
	setAnalyzeState();
}

function setAnalyzeState() {
	if ((typeof breed !== 'undefined') ||
		(typeof age !== 'undefined') ||
		($("#signs input[type='checkbox']:checked").length > 0)) {
		$('#check').removeClass('disabled');
	} else {
		$('#check').addClass('disabled');
	}
}

function buildBreedsDropdown() {
	$('#breed-select .menu').append('<div data-value="none" class="item">' + locales[lang]['no-breed'] + '</div>')
	var options = '';
	for (var i=0; i<UIdata.breeds.length; i++) {
		var breed_opt = UIdata.breeds[i];
		var breed_str = breed_opt;
		if (UIdata.aliases[breed_opt] && (UIdata.aliases[breed_opt].length !== 0)) {
			breed_str += '<div class="breed_aliases">' + UIdata.aliases[breed_opt].join(', ') + '</div>';
		}
		options += '<div class="item" data-value="' + breed_opt + '">' + breed_str + "</div>";
	}
	$('#breed-select .menu').append(options);
}

function buildBreedsIIDropdown() {
	var options = '';
	for (var i=0; i<UIdata.breeds.length; i++) {
		var breed_opt = UIdata.breeds[i];
		var breed_str = breed_opt;
		if (UIdata.aliases[breed_opt] && (UIdata.aliases[breed_opt].length !== 0)) {
			breed_str += '<div class="breed_aliases">' + UIdata.aliases[breed_opt].join(', ') + '</div>';
		}
		options += '<div class="item" data-value="' + breed_opt + '">' + breed_str + "</div>";
	}
	$('#breedII-select .menu').append(options);
}

function buildAgeDropdown() {
	var ages = ''
	for (var i=1; i<21; i++) {
		ages += '<option value="' + i + '">' + i + '</option>';
	}
	$('#age-select').append(ages);
}

function buildSymptomsCB() {
	var symptoms = '';
	var row = 0;
	for (var symptom in UIdata['symptoms']) {
		symptomStr = symptom;
		if (lang != 'en-US') {
			symptomStr = locales[lang].symptoms[symptom];
		}
		if ((row % 2) === 0) {
			if (row > 0) {
				symptoms += '<div class="ui divider"></div>';
			}
			symptoms += '<div class="row">';
		}

		symptoms += '<div class="column">';
		symptoms += '<label>' + symptomStr + '</label>';
		var description = UIdata['symptoms'][symptom].description;
		if (lang != 'en-US') {
			description = locales[lang]['symptoms-description'][symptom];
		}
		if (typeof description !== 'undefined') {
			symptoms += '<br/><span>' + description + '</span>';
		} 
		symptoms += '</div>';
		symptoms += '<div class="column">';
		symptoms += '<div class="ui toggle checkbox">';
		symptoms += '<input type="checkbox" name="' + symptom + '">';
		symptoms += '</div>';
		symptoms += '</div>';
		if ((row % 2) === 0) {
			symptoms += '</div>';
		}
		row+=2;
	}
	$('#signs').append(symptoms);
}

function getDiseasesBySymptoms(symptomsArr) {
	if (typeof symptomsArr === 'undefined') return;

	var diseases = {};
	var result = [];
	var symptoms = {};

	for (var i=0; i<symptomsArr.length; i++) {
		symptoms[symptomsArr[i]] = true;
	}

	for (disease in dogScreening['diseases']) {
		var diseasesSymptoms = dogScreening['diseases'][disease].symptoms;
		for (i=0; i<diseasesSymptoms.length; i++) {
			if (typeof symptoms[diseasesSymptoms[i]] !== 'undefined' &&
				typeof diseases[disease] === 'undefined') {
				diseases[disease] = true;
				result.push(disease);
			}
		}
	}
	return result;
}

function buildResultsDetails(severity, results) {
		var disease;
		var details = '';
		var text = {
			diseasesByBreed: '',
			diseasesByBreedAndSymptoms: '',
			diseasesBySymptomsRest: '',
			diseasesByGenderPrevalance: ''
		};
		var i;

		$('#traffic-light #' + severity + ' .details').empty();

		for (arr in text) {
			for (i=0; i<results[arr].length; i++) {
				disease = results[arr][i];
				var link = results.diseasesLinks[disease] ? results.diseasesLinks[disease]  : '#';
				text[arr] += '<a href="' + link + '" target="_blank">' + disease + '</a>, ';
			}
			text[arr] = text[arr].slice(0, -2); // remove last coma
		}

		var str = locales[lang];

		if (age > 8) {
			details += '<p><i class="icon info circle red"></i>';
			if (age < 13) {
				details += $.tmpl(str['results-details-age-risks-group-1']).text();
			} else if (age < 16) {
				details += $.tmpl(str['results-details-age-risks-group-2']).text();
			} else {
				details += $.tmpl(str['results-details-age-risks-group-3']).text();
			}
			details += '</p>';
		}
		if (results.diseasesByBreed.length) {
			details += '<p>'; 
			details += $.tmpl(str['results-details-breed-risks'], {'diseases': text.diseasesByBreed}).text(); 
			details += '</p>';
		}

		if (results.diseasesByBreedAndSymptoms.length) {
			details += '<p><i class="icon info circle red"></i>';
			details += $.tmpl(str['results-details-breed-and-symptoms-risks'], {'diseases': text.diseasesByBreedAndSymptoms}).text(); 
			details += '</p>';
		}
		if (results.diseasesBySymptomsRest.length) {
			details += '<p>';
			if (results.diseasesByBreedAndSymptoms.length > 0) {
				details += $.tmpl(str['results-details-symptoms-rest-risks'], {'diseases': text.diseasesBySymptomsRest}).text(); 
			} else {
				details += $.tmpl(str['results-details-symptoms-risks'], {'diseases': text.diseasesBySymptomsRest}).text(); 
			}
			details += '</p>';
		}
		if (results.diseasesByGenderPrevalance.length) {
			var gender;
			if (isMale) {
				gender = $.tmpl(str['results-details-males']).text();
			} else {
				gender = $.tmpl(str['results-details-females']).text();
			}
			details += '<p><i class="icon info circle red"></i>';
			details += $.tmpl(str['results-details-gender-risks'], {
				'diseases': text.diseasesByGenderPrevalance, 
				'gender': gender}).text();
			details += '</p>';
		}
		
		var detailsDiv = $('#traffic-light #' + severity + ' .details');
		if (details !== '') {
			detailsDiv.append(details);
			detailsDiv.show();
		} else {
			detailsDiv.hide();
		}
}

function hideResults() {
	$("#results").hide();
	$("#traffic-light .segment").hide();
}

function onGotResults(results) {	
	$('#check').removeClass('disabled').removeClass('loading');

	if (results.severity !== 'green') {
		buildResultsDetails(results.severity, results.data);
	}

	$('#' + results.severity).show();

	$('#results').show();
	scrollToBottom();
}

function onCheck(source) {
	hideResults();
	updateUrlHash();
	$('#check').addClass('disabled').addClass('loading');

	var symptoms = [];
	$.each($("#signs input[type='checkbox']:checked"), function() {
		symptoms.push($(this).prop('name'));
	});

	$.ajax({
			type: "POST",
			url: "https://jtkfcy5onl.execute-api.us-east-1.amazonaws.com/prod/screening-results",
			data: JSON.stringify({
				breed: breed,
				breedII: breedII,
				age: age,
				isMale: isMale,
				isFemale: isFemale,
				symptoms: symptoms,
				lang: lang,
				version: version,
				source: source
			}),
			success: onGotResults,
			contentType: 'application/json; charset=utf-8',
			dataType: "json"
	});
}

function onClear() {
	hideResults();
	$('#breed-select').dropdown('clear');
	$('#age-select').dropdown('clear');
	$('#gender-select').dropdown('clear');
    $.each($("#signs input[type='checkbox']:checked"), function() {
		$(this).parent().checkbox('uncheck');	
	});
	window.location.replace(('' + window.location).split('#')[0] + '#');
}

function addBreedIIOption(e) {
	breedIIEnabled = true;
	$('#mixed-breed-add').hide();
	$('#mixed-breed-remove').show();
	$('#breedII-row').css('display', 'flex');
	breedII = $('#breedII-select').dropdown('get value');
	
	e.preventDefault();
}

function removeBreedIIOption(e) {
	breedIIEnabled = false;
	$('#mixed-breed-remove').hide();
	$('#mixed-breed-add').show();
	$('#breedII-row').hide();
	breedII = 'none';

	e.preventDefault();

}

function onSignsClicked(e) {
	var target = $(e.target);
	if (target.hasClass('checkbox') || target.parents('.checkbox').length) return;
	var row;
	if (target.parents('.row').length) {
		row = target.parents('.row')[0];
	} else if (target.hasClass('row')) {
		row = target;
	}
	if (row) {
		$(row).find('.checkbox').checkbox('toggle');
	}
}

function scrollToBottom() {
	var page = $("html, body");

	page.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
		page.stop();
	});

	page.animate({ scrollTop: $(document).height() }, 1000, function(){
		page.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
	});

	 //$("html, body").animate({ scrollTop: $(document).height() }, 2000);
	 //$("html, body").scrollTop( $(document).height() );
}

function populateStrings() {
	var str = locales[lang];
	if (lang === 'he') {
		$('body').addClass('rtl');
	}
	$('#header h1').text(str['page-title']);
	$('#header h4:eq(0)').text(str['page-subtitle1']);
	$('#header h4:eq(1)').text(str['page-subtitle2']);
	$('#info-title h2').text(str['info-title']);
	$('#breed-label').text(str['breed-label']);
	$('#breedII-label').text(str['breedII-label']);
	$('#mixed-breed-add span').text(str['mixed-breed-add-label']);
	$('#mixed-breed-add a').text(str['mixed-breed-add-action']);
	$('#mixed-breed-remove a').text(str['mixed-breed-remove-action']);
	$('#age-label').text(str['age-label']);
	$('#gender-label').text(str['gender-label']);
	$('#gender-select option[value="male"]').text(str['gender-male']);
	$('#gender-select option[value="female"]').text(str['gender-female']);
	$('#signs-title h2').text(str['signs-title']);
	$('#check').text(str['check-label']);
	$('#clear').text(str['clear-label']);
	$('#green .header').text(str['green-title']);
	$('#green .content p').text(str['green-subtitle']);
	$('#yellow .header').text(str['yellow-title']);
	$('#yellow .content p').text(str['yellow-subtitle']);
	$('#red .header').text(str['red-title']);
	$('#red .content p').html(str['red-subtitle']);
	$('#share label').text(str['share-label']);
	$('#about').text(str['about-us']);
	$('#terms').text(str['terms-of-use']);
	$('#privacy').text(str['privacy-policy']);
	$('#copyrights').text(str['copyrights']);
	$('#contact').text(str['contact-us']);
}

$(document).ready(function () {
	populateStrings();


	buildBreedsDropdown();
	buildBreedsIIDropdown();
	buildAgeDropdown();
	buildSymptomsCB();

	$('#breed-select').dropdown({
		onChange: onBreedSelected,
		preserveHTML: true,
		fullTextSearch: 'exact',
		clearable: true
	});
	$('#breed-select').dropdown('set selected', 'none');

	$('#breedII-select').dropdown({
		onChange: onBreedIISelected,
		preserveHTML: true,
		fullTextSearch: 'exact',
		clearable: true
	});
	
	$('#age-select').dropdown({
		onChange: onAgeSelected,
		clearable: true
	});
	$('#gender-select').dropdown({
		onChange: onGenderSelected,
		clearable: true
	});

	$('#mixed-breed-add a').on('click', addBreedIIOption)
	$('#mixed-breed-remove a').on('click', removeBreedIIOption)


	$('#signs .checkbox').checkbox({
			onChecked: onSymptomChecked,
			onUnchecked: onSymptomUnchecked
	});
	$('.ui.accordion').accordion({
		onOpen: scrollToBottom
	});
	$('#signs').on('click', onSignsClicked)
	$('#check').on('click', function () { onCheck('click') });
	$('#clear').on('click', onClear);

	var clipboard = new ClipboardJS('.copy');
	clipboard.on('success', function(e) {
		var copyIcon = $('.copy');
		var opt = {
			title: $.tmpl(locales[lang]['copied-to-clipboard']).text(),
            on: 'manual',
            exclusive: true,
            distanceAway: 18
		}
		if ($('body').hasClass('rtl')) {
			opt.arrowPixelsFromEdge = 35;
		}
   		copyIcon.popup(opt).popup('show');
   		setTimeout(function() { copyIcon.popup('hide') }, 1500);
	});

	updateFromHashParams();
	setAnalyzeState();
})