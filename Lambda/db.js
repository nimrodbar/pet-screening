const DynamoDB = require('aws-sdk/clients/dynamodb')

const dynamoDB = new DynamoDB.DocumentClient({ convertEmptyValues: true })

module.exports.put = async (request, response, context) => {
  return dynamoDB.put({
    TableName: "dog-screening",
    Item: {
      id: "id",
      timestamp: new Date().toISOString() + "_" + (Math.floor(Math.random() * Math.floor(1000))),
      request,
      response,
      context
    }
  }).promise()
