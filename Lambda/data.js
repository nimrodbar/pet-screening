module.exports = {

  "breeds": {
    "Afghan Hound": {
      "diseases": [
        "Dilated Cardiomyopathy",
        "Cardiac Tumors"
      ]
    },
    "Airedale Terrier": {
      "diseases": [
        "Pulmonic Stenosis"
      ]
    },
    "Akita": {},
    "Alaskan Malamute": {},
    "Alpine Dachsbracke": {},
    "American Akita": {},
    "American Cocker Spaniel": {
      "diseases": [
        "Pulmonic Stenosis",
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Ischemic Heart Disease",
        "Cardiac Tumors"
      ]
    },
    "American Foxhound": {},
    "American Staffordshire Terrier": {},
    "American Water Spaniel": {},
    "Appenzell Cattle Dog": {},
    "Ariege Pointing Dog": {},
    "Ariegeois": {},
    "Artois Hound": {},
    "Atlas Mountain Dog (aidi)": {},
    "Australian Cattle Dog": {},
    "Australian Kelpie": {},
    "Australian Shepherd": {},
    "Australian Silky Terrier": {},
    "Australian Stumpy Tail Cattle Dog": {},
    "Australian Terrier": {},
    "Austrian  Pinscher": {},
    "Austrian Black And Tan Hound": {},
    "Auvergne Pointer": {},
    "Azawakh": {},
    "Basenji": {},
    "Basset Fauve De Bretagne": {},
    "Basset Hound": {},
    "Bavarian Mountain Scent Hound": {},
    "Beagle": {
      "diseases": [
        "Pulmonic Stenosis"
      ]
    },
    "Beagle Harrier": {},
    "Bearded Collie": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Bedlington Terrier": {},
    "Belgian Shepherd Dog": {},
    "Bergamasco Shepherd Dog": {},
    "Berger De Beauce": {},
    "Bernese Mountain Dog": {},
    "Bichon Frise": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Billy": {},
    "Black And Tan Coonhound": {},
    "Bloodhound": {},
    "Blue Gascony Basset": {},
    "Blue Gascony Griffon": {},
    "Blue Picardy Spaniel": {},
    "Bohemian Wire-haired Pointing Griffon": {},
    "Bolognese": {},
    "Border Collie": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Border Terrier": {},
    "Borzoi - Russian Hunting Sighthound": {},
    "Bosnian And Herzegovinian - Croatian Shepherd Dog": {},
    "Bosnian Broken-haired Hound - Called Barak": {},
    "Boston Terrier": {
      "diseases": [
        "Degenerative Valve Disease",
        "Cardiac Tumors"
      ]
    },
    "Bourbonnais Pointing Dog": {},
    "Bouvier Des Ardennes": {},
    "Bouvier Des Flandres": {
      "diseases": [
        "Subaortic Stenosis"
      ]
    },
    "Boxer": {
      "diseases": [
        "Subaortic Stenosis",
        "Pulmonic Stenosis",
        "Atrial Septal Defect",
        "Tricuspid Valve Dysplasia",
        "Dilated Cardiomyopathy",
        "Boxer Myocardial Disease",
        "Cardiac Tumors"
      ]
    },
    "Boykin Spaniel": {
      "diseases": [
        "Pulmonic Stenosis"
      ]
    },
    "Brazilian Terrier": {},
    "Briard": {},
    "Briquet Griffon Vendeen": {},
    "Brittany Spaniel": {},
    "Broholmer": {},
    "Bull Terrier": {
      "diseases": [
        "Mitral Valve Dysplasia"
      ]
    },
    "Bulldog": {
      "diseases": [
        "Dilated Cardiomyopathy",
        "Cardiac Tumors",
        "Subaortic Stenosis",
        "Pulmonic Stenosis",
        "Ventricular Septal Defect",
        "Tetralogy Of Fallot"
      ]
    },
    "Bullmastiff": {},
    "Burgos Pointing Dog": {},
    "Cairn Terrier": {},
    "Canaan Dog": {},
    "Canadian Eskimo Dog": {},
    "Canarian Warren Hound": {},
    "Castro Laboreiro Dog": {},
    "Catalan Sheepdog": {},
    "Caucasian Shepherd Dog": {},
    "Cavalier King Charles Spaniel": {
      "diseases": [
        "Degenerative Valve Disease",
        "Ischemic Heart Disease"
      ]
    },
    "Central Asia Shepherd Dog": {},
    "Cesky Terrier": {},
    "Chesapeake Bay Retriever": {},
    "Chihuahua": {
      "diseases": [
        "Pulmonic Stenosis",
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease"
      ]
    },
    "Chinese Crested Dog": {},
    "Chow Chow": {},
    "Cimarrón Uruguayo": {},
    "Cirneco Dell'etna": {},
    "Clumber Spaniel": {},
    "Coarse-haired Styrian Hound": {},
    "Cocker Spaniel": {
      "diseases": [
        "Pulmonic Stenosis",
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Ischemic Heart Disease",
        "Cardiac Tumors"
      ]
    },
    "Collie": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Collie Rough": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Collie Smooth": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Continental Toy Spaniel": {},
    "Coton De Tulear": {},
    "Croatian Shepherd Dog": {},
    "Curly Coated Retriever": {},
    "Czechoslovakian Wolfdog": {},
    "Dachshund": {},
    "Dalmatian": {
      "diseases": [
        "Dilated Cardiomyopathy"
      ]
    },
    "Dandie Dinmont Terrier": {},
    "Danish-swedish Farmdog": {},
    "Deutsch Langhaar": {},
    "Deutsch Stichelhaar": {},
    "Doberman Pinscher": {
      "diseases": [
        "Atrial Septal Defect",
        "Dilated Cardiomyopathy",
        "Cardiac Tumors"
      ]
    },
    "Dogo Argentino": {},
    "Dogo Canario": {},
    "Dogue De Bordeaux": {},
    "Drentsche Partridge Dog": {},
    "Drever": {},
    "Dutch Schapendoes": {},
    "Dutch Shepherd Dog": {},
    "Dutch Smoushond": {},
    "East Siberian Laika": {},
    "English Cocker Spaniel": {
      "diseases": [
        "Pulmonic Stenosis",
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Ischemic Heart Disease",
        "Cardiac Tumors"
      ]
    },
    "English Foxhound": {},
    "English Pointer": {},
    "English Setter": {
      "diseases": [
        "Cardiac Tumors"
      ]
    },
    "English Springer Spaniel": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Ventricular Septal Defect"
      ]
    },
    "English Toy Terrier (black &tan)": {},
    "Entlebuch Cattle Dog": {},
    "Estrela Mountain Dog": {},
    "Eurasian": {},
    "Fawn Brittany Griffon": {},
    "Field Spaniel": {},
    "Fila Brasileiro": {},
    "Finnish Hound": {},
    "Finnish Lapponian Dog": {},
    "Finnish Spitz": {},
    "Flat Coated Retriever": {},
    "Fox Terrier": {
      "diseases": [
        "Pulmonic Stenosis",
        "Degenerative Valve Disease"
      ]
    },
    "Fox Terrier (smooth)": {},
    "Fox Terrier (wire)": {},
    "French Bulldog": {},
    "French Pointing Dog - Gascogne Type": {},
    "French Pointing Dog - Pyrenean Type": {},
    "French Spaniel": {},
    "French Tricolour Hound": {},
    "French Water Dog": {},
    "French White & Black Hound": {},
    "French White And Orange Hound": {},
    "Frisian Water Dog": {},
    "Gascon Saintongeois": {},
    "German Hound": {},
    "German Hunting Terrier": {},
    "German Pinscher": {},
    "German Shepherd Dog": {
      "diseases": [
        "Subaortic Stenosis",
        "Patent Ductus Arteriosus",
        "Mitral Valve Dysplasia",
        "Tricuspid Valve Dysplasia",
        "Persistent Right Aortic Arch",
        "Degenerative Valve Disease",
        "Infective Endocarditis",
        "Constrictive Pericardial Disease",
        "Cardiac Tumors"
      ]
    },
    "German Short- Haired Pointing Dog": {},
    "German Shorthaired Pointer": {
      "diseases": [
        "Subaortic Stenosis"
      ]
    },
    "German Spaniel": {},
    "German Spitz": {},
    "German Wire- Haired Pointing Dog": {},
    "Giant Schnauzer": {},
    "Golden Retriever": {
      "diseases": [
        "Subaortic Stenosis",
        "Mitral Valve Dysplasia",
        "Tricuspid Valve Dysplasia",
        "Cardiac Tumors"
      ]
    },
    "Gordon Setter": {},
    "Grand Basset Griffon Vendeen": {},
    "Grand Griffon Vendeen": {},
    "Great Anglo-french Tricolour Hound": {},
    "Great Anglo-french White & Orange Hound": {},
    "Great Anglo-french White And Black Hound": {},
    "Great Dane": {
      "diseases": [
        "Subaortic Stenosis",
        "Mitral Valve Dysplasia",
        "Tricuspid Valve Dysplasia",
        "Persistent Right Aortic Arch",
        "Dilated Cardiomyopathy"
      ]
    },
    "Great Gascony Blue": {},
    "Great Swiss Mountain Dog": {},
    "Greenland Dog": {},
    "Greyhound": {},
    "Griffon Belge": {},
    "Griffon Bruxellois": {},
    "Griffon Nivernais": {},
    "Halden Hound": {},
    "Hamiltonstövare": {},
    "Hanoverian Scent Hound": {},
    "Harrier": {},
    "Havanese": {},
    "Hellenic Hound": {},
    "Hokkaido": {},
    "Hovawart": {},
    "Hungarian Greyhound": {},
    "Hungarian Hound - Transylvanian Scent Hound": {},
    "Hungarian Short-haired Pointer (vizsla)": {},
    "Hungarian Wire-haired Pointer": {},
    "Hygen Hound": {},
    "Ibizan Podenco": {},
    "Icelandic Sheepdog": {},
    "Irish Glen Of Imaal Terrier": {},
    "Irish Red And White Setter": {
      "diseases": [
        "Persistent Right Aortic Arch"
      ]
    },
    "Irish Red Setter": {
      "diseases": [
        "Persistent Right Aortic Arch"
      ]
    },
    "Irish Setter": {
      "diseases": [
        "Persistent Right Aortic Arch"
      ]
    },
    "Irish Soft Coated Wheaten Terrier": {},
    "Irish Terrier": {},
    "Irish Water Spaniel": {},
    "Irish Wolfhound": {
      "diseases": [
        "Dilated Cardiomyopathy"
      ]
    },
    "Istrian Short-haired Hound": {},
    "Istrian Wire-haired Hound": {},
    "Italian Cane Corso": {},
    "Italian Pointing Dog": {},
    "Italian Rough-haired Segugio": {},
    "Italian Short-haired Segugio": {},
    "Italian Sighthound": {},
    "Italian Spinone": {},
    "Italian Volpino": {},
    "Jack Russell Terrier": {},
    "Jämthund": {},
    "Japanese Chin": {},
    "Japanese Spitz": {},
    "Japanese Terrier": {},
    "Kai": {},
    "Kangal Shepherd Dog": {},
    "Karelian Bear Dog": {},
    "Karst Shepherd Dog": {},
    "Keeshond": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Ventricular Septal Defect",
        "Tetralogy Of Fallot"
      ]
    },
    "Kerry Blue Terrier": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "King Charles Spaniel": {},
    "Kishu": {},
    "Kleiner Münsterländer": {},
    "Komondor": {},
    "Korea Jindo Dog": {},
    "Kromfohrländer": {},
    "Kuvasz": {},
    "Labrador Retriever": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Tricuspid Valve Dysplasia",
        "Cardiac Tumors"
      ]
    },
    "Lakeland Terrier": {},
    "Lancashire Heeler": {},
    "Landseer (european Continental Type)": {},
    "Lapponian Herder": {},
    "Large Munsterlander": {},
    "Leonberger": {},
    "Lhasa Apso": {},
    "Little Lion Dog": {},
    "Long-haired Pyrenean Sheepdog": {},
    "Majorca Mastiff": {},
    "Majorca Shepherd Dog": {},
    "Maltese": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Manchester Terrier": {},
    "Maremma And The Abruzzes Sheepdog": {},
    "Mastiff": {
      "diseases": [
        "Pulmonic Stenosis",
        "Mitral Valve Dysplasia"
      ]
    },
    "Medium-sized Anglo-french Hound": {},
    "Miniature Bull Terrier": {},
    "Miniature Pinscher": {
      "diseases": [
        "Degenerative Valve Disease"
      ]
    },
    "Miniature Poodle": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease",
        "Cardiac Tumors"
      ]
    },
    "Miniature Schnauzer": {
      "diseases": [
        "Pulmonic Stenosis",
        "Degenerative Valve Disease"
      ]
    },
    "Montenegrin Mountain Hound": {},
    "Mudi": {},
    "Neapolitan Mastiff": {},
    "Nederlandse Kooikerhondje": {},
    "Newfoundland": {
      "diseases": [
        "Subaortic Stenosis",
        "Patent Ductus Arteriosus",
        "Mitral Valve Dysplasia",
        "Dilated Cardiomyopathy"
      ]
    },
    "Norfolk Terrier": {},
    "Norman Artesien Basset": {},
    "Norrbottenspitz": {},
    "Norwegian Buhund": {},
    "Norwegian Elkhound Black": {},
    "Norwegian Elkhound Grey": {},
    "Norwegian Hound": {},
    "Norwegian Lundehund": {},
    "Norwich Terrier": {},
    "Nova Scotia Duck Tolling Retriever": {},
    "Old Danish Pointing Dog": {},
    "Old English Sheepdog": {
      "diseases": [
        "Tricuspid Valve Dysplasia"
      ]
    },
    "Otterhound": {},
    "Parson Russell Terrier": {},
    "Pekingese": {
      "diseases": [
        "Degenerative Valve Disease"
      ]
    },
    "Peruvian Hairless Dog": {},
    "Petit Basset Griffon Vendeen": {},
    "Petit Brabançon": {},
    "Pharaoh Hound": {},
    "Picardy Sheepdog": {},
    "Picardy Spaniel": {},
    "Poitevin": {},
    "Polish Greyhound": {},
    "Polish Hound": {},
    "Polish Hunting Dog": {},
    "Polish Lowland Sheepdog": {},
    "Pomeranian": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease"
      ]
    },
    "Pont-audemer Spaniel": {},
    "Poodle": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease",
        "Cardiac Tumors"
      ]
    },
    "Porcelaine": {},
    "Portuguese Pointing Dog": {},
    "Portuguese Sheepdog": {},
    "Portuguese Warren Hound-portuguese Podengo": {},
    "Portuguese Water Dog": {
      "diseases": [
        "Dilated Cardiomyopathy"
      ]
    },
    "Posavatz Hound": {},
    "Pudelpointer": {},
    "Pug": {},
    "Puli": {},
    "Pumi": {},
    "Pyrenean Mastiff": {},
    "Pyrenean Mountain Dog": {},
    "Pyrenean Sheepdog - Smooth Faced": {},
    "Rafeiro Of Alentejo": {},
    "Rhodesian Ridgeback": {},
    "Romagna Water Dog": {},
    "Romanian Bucovina Shepherd": {},
    "Romanian Carpathian Shepherd Dog": {},
    "Romanian Mioritic Shepherd Dog": {},
    "Rottweiler": {
      "diseases": [
        "Subaortic Stenosis",
        "Mitral Valve Dysplasia"
      ]
    },
    "Russian Black Terrier": {},
    "Russian Toy": {},
    "Russian-european Laika": {},
    "Saarloos Wolfhond": {},
    "Saint Germain Pointer": {},
    "Saint Miguel Cattle Dog": {},
    "Saluki": {},
    "Samoyed": {
      "diseases": [
        "Dilated Cardiomyopathy"
      ]
    },
    "Schillerstövare": {},
    "Schipperke": {},
    "Schnauzer": {},
    "Scottish Deerhound": {
      "diseases": [
        "Subaortic Stenosis",
        "Pulmonic Stenosis",
        "Atrial Septal Defect"
      ]
    },
    "Scottish Terrier": {
      "diseases": [
        "Dilated Cardiomyopathy"
      ]
    },
    "Sealyham Terrier": {},
    "Segugio Maremmano": {},
    "Serbian Hound": {},
    "Serbian Tricolour Hound": {},
    "Shar Pei": {},
    "Shetland Sheepdog": {
      "diseases": [
        "Pulmonic Stenosis"
      ]
    },
    "Shiba": {},
    "Shih Tzu": {},
    "Shikoku": {},
    "Siberian Husky": {},
    "Skye Terrier": {},
    "Sloughi": {},
    "Slovakian Chuvach": {},
    "Slovakian Hound": {},
    "Smålandsstövare": {},
    "Small Blue Gascony": {},
    "Small Swiss Hound": {},
    "South Russian Shepherd Dog": {},
    "Spanish Greyhound": {},
    "Spanish Hound": {},
    "Spanish Mastiff": {},
    "Spanish Water Dog": {},
    "St. Bernard": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Stabijhoun": {},
    "Staffordshire Bull Terrier": {},
    "Sussex Spaniel": {},
    "Swedish Lapphund": {},
    "Swedish Vallhund": {},
    "Swiss Hound": {},
    "Taiwan Dog": {},
    "Tatra Shepherd Dog": {},
    "Thai Bangkaew Dog": {},
    "Thai Ridgeback Dog": {},
    "Tibetan Mastiff": {},
    "Tibetan Spaniel": {},
    "Tibetan Terrier": {},
    "Tosa": {},
    "Toy Poodle": {
      "diseases": [
        "Patent Ductus Arteriosus",
        "Degenerative Valve Disease"
      ]
    },
    "Tyrolean Hound": {},
    "Weimaraner": {
      "diseases": [
        "Congenital Pericardial Malformations",
        "Tricuspid Valve Dysplasia"
      ]
    },
    "Welsh Corgi (cardigan)": {},
    "Welsh Corgi (pembroke)": {},
    "Welsh Springer Spaniel": {},
    "Welsh Terrier": {},
    "West Highland White Terrier": {
      "diseases": [
        "Pulmonic Stenosis"
      ]
    },
    "West Siberian Laika": {},
    "Westphalian Dachsbracke": {},
    "Whippet": {
      "diseases": [
        "Degenerative Valve Disease"
      ]
    },
    "White Swiss Shepherd Dog": {},
    "Wire-haired Pointing Griffon Korthals": {},
    "Wirehaired Slovakian Pointer": {},
    "Xoloitzcuintle": {},
    "Yorkshire Terrier": {
      "diseases": [
        "Patent Ductus Arteriosus"
      ]
    },
    "Yugoslavian Shepherd Dog - Sharplanina": {}
  },
  "diseases": {
    "Atrial Septal Defect": {
      "symptoms": [
        "Murmur History",
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Pale Tongue/White Tongue And Pale Gums"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Septal",
      "link": "https://en.wikipedia.org/wiki/Atrial_septal_defect"
    },
    "Boxer Myocardial Disease": {
      "symptoms": [
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Changes In Behavior"
      ],
      "congenital": false,
      "acquired": true,
      "group": "Myocardial",
      "link": "https://en.wikipedia.org/wiki/Boxer_cardiomyopathy"
    },
    "Cardiac Tumors": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Changes In Behavior",
        "Weakness",
        "Changes In Behavior"
      ],
      "congenital": false,
      "acquired": true,
      "group": "Infective",
      "link": "https://en.wikipedia.org/wiki/Heart_cancer"
    },
    "Congenital Pericardial Malformations": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Changes In Behavior"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Pericardial",
      "link": ""
    },
    "Constrictive Pericardial Disease": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Exercise Intolerance",
        "Weakness"
      ],
      "congenital": false,
      "acquired": true,
      "group": "Pericardial",
      "link": "https://en.wikipedia.org/wiki/Constrictive_pericarditis"
    },
    "Degenerative Valve Disease": {
      "symptoms": [
        "Murmur History",
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Weight Loss",
        "Weakness",
        "Loss Of Appetite",
        "Changes In Behavior"
      ],
      "males": [
        "Miniature Pinscher"
      ],
      "females": [],
      "congenital": false,
      "acquired": true,
      "group": "Valvular",
      "link": "https://en.wikipedia.org/wiki/Valvular_heart_disease"
    },
    "Dilated Cardiomyopathy": {
      "symptoms": [
        "Murmur History",
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Pale Tongue/White Tongue And Pale Gums",
        "Weakness",
        "Loss Of Appetite"
      ],
      "males": [
        "Afghan Hound",
        "Boxer",
        "Bulldog",
        "Cocker Spaniel",
        "Dalmatian",
        "Doberman Pinscher",
        "Great Dane",
        "Irish Wolfhound",
        "Newfoundland",
        "Portuguese Water Dog",
        "Saint Bernard",
        "Scottish Deerhound"
      ],
      "females": [],
      "congenital": false,
      "acquired": true,
      "group": "Myocardial",
      "link": "https://en.wikipedia.org/wiki/Dilated_cardiomyopathy"
    },
    "Infective Endocarditis": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Exercise Intolerance",
        "Exercise Intolerance",
        "Changes In Behavior",
        "Weakness",
        "Loss Of Appetite"
      ],
      "congenital": false,
      "acquired": true,
      "group": "Infective",
      "link": "https://en.wikipedia.org/wiki/Infective_endocarditis"
    },
    "Ischemic Heart Disease": {
      "symptoms": [
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Weakness",
        "Loss Of Appetite",
        "Changes In Behavior"
      ],
      "congenital": false,
      "acquired": true,
      "group": "Myocardial",
      "link": "https://en.wikipedia.org/wiki/Coronary_artery_disease"
    },
    "Mitral Valve Dysplasia": {
      "symptoms": [
        "Murmur History",
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Exercise Intolerance",
        "Weakness"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Valvular",
      "link": "https://en.wikipedia.org/wiki/Heart_valve_dysplasia"
    },
    "Patent Ductus Arteriosus": {
      "symptoms": [
        "Murmur History",
        "Frequent Coughing",
        "Difficulty Breathing",
        "Weakness"
      ],
      "males": [],
      "females": [
        "Bichon Frise",
        "Chihuahua",
        "Cocker Spaniel",
        "Collie",
        "English Springer Spaniel",
        "German Shepherd Dog",
        "Keeshond",
        "Kerry Blue Terrier",
        "Labrador Retriever",
        "Maltese",
        "Miniature Poodle",
        "Newfoundland",
        "Pomeranian",
        "Shetland Sheepdog",
        "Toy Poodle",
        "Yorkshire Terrier"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Aortic",
      "link": "https://en.wikipedia.org/wiki/Patent_ductus_arteriosus"
    },
    "Persistent Right Aortic Arch": {
      "symptoms": [
        "Difficulty Breathing",
        "Difficulty Eating Solid Food"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Aortic",
      "link": "https://en.wikipedia.org/wiki/Right-sided_aortic_arch"
    },
    "Pulmonic Stenosis": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Swollen Abdomen",
        "Exercise Intolerance"
      ],
      "males": [
        "Bulldog"
      ],
      "females": [],
      "congenital": true,
      "acquired": false,
      "group": "Valvular",
      "link": "https://en.wikipedia.org/wiki/Pulmonic_stenosis"
    },
    "Subaortic Stenosis": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Weakness"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Valvular",
      "link": "https://en.wikipedia.org/wiki/Aortic_stenosis"
    },
    "Tetralogy Of Fallot": {
      "symptoms": [
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Pale Tongue/White Tongue And Pale Gums",
        "Weakness"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Septal",
      "link": "https://en.wikipedia.org/wiki/Tetralogy_of_Fallot"
    },
    "Tricuspid Valve Dysplasia": {
      "symptoms": [
        "Murmur History",
        "Difficulty Breathing",
        "Swollen Abdomen",
        "Exercise Intolerance",
        "Weakness"
      ],
      "males": [
        "Boxer",
        "German Shepherd Dog",
        "Golden Retriever",
        "Great Dane",
        "Labrador Retriever",
        "Old English Sheepdog",
        "Weimaraner"
      ],
      "females": [],
      "congenital": true,
      "acquired": false,
      "group": "Valvular",
      "link": "https://en.wikipedia.org/wiki/Heart_valve_dysplasia"
    },
    "Ventricular Septal Defect": {
      "symptoms": [
        "Murmur History",
        "Frequent Coughing",
        "Difficulty Breathing",
        "Fainting, Collapsing",
        "Exercise Intolerance"
      ],
      "congenital": true,
      "acquired": false,
      "group": "Septal",
      "link": "https://en.wikipedia.org/wiki/Ventricular_septal_defect"
    }
  },
  "symptoms": {
    "Murmur History": {
      "diseases": [
        "Atrial Septal Defect",
        "Cardiac Tumors",
        "Congenital Pericardial Malformations",
        "Constrictive Pericardial Disease",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Infective Endocarditis",
        "Mitral Valve Dysplasia",
        "Patent Ductus Arteriosus",
        "Pulmonic Stenosis",
        "Subaortic Stenosis",
        "Tricuspid Valve Dysplasia",
        "Ventricular Septal Defect"
      ],
      "severity": "low"
    },
    "Frequent Coughing": {
      "diseases": [
        "Atrial Septal Defect",
        "Boxer Myocardial Disease",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Ischemic Heart Disease",
        "Mitral Valve Dysplasia",
        "Patent Ductus Arteriosus",
        "Ventricular Septal Defect"
      ],
      "severity": "high"
    },
    "Difficulty Breathing": {
      "diseases": [
        "Atrial Septal Defect",
        "Boxer Myocardial Disease",
        "Cardiac Tumors",
        "Congenital Pericardial Malformations",
        "Constrictive Pericardial Disease",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Infective Endocarditis",
        "Ischemic Heart Disease",
        "Mitral Valve Dysplasia",
        "Patent Ductus Arteriosus",
        "Persistent Right Aortic Arch",
        "Pulmonic Stenosis",
        "Subaortic Stenosis",
        "Tetralogy Of Fallot",
        "Tricuspid Valve Dysplasia",
        "Ventricular Septal Defect"
      ],
      "severity": "high",
      "description": "Fast (not panting) or laboured breathing"
    },
    "Fainting, Collapsing": {
      "diseases": [
        "Atrial Septal Defect",
        "Boxer Myocardial Disease",
        "Cardiac Tumors",
        "Congenital Pericardial Malformations",
        "Constrictive Pericardial Disease",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Ischemic Heart Disease",
        "Mitral Valve Dysplasia",
        "Subaortic Stenosis",
        "Tetralogy Of Fallot",
        "Ventricular Septal Defect"
      ],
      "severity": "high"
    },
    "Swollen Abdomen": {
      "diseases": [
        "Atrial Septal Defect",
        "Boxer Myocardial Disease",
        "Congenital Pericardial Malformations",
        "Constrictive Pericardial Disease",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Ischemic Heart Disease",
        "Pulmonic Stenosis",
        "Tricuspid Valve Dysplasia"
      ],
      "severity": "high"
    },
    "Exercise Intolerance": {
      "diseases": [
        "Atrial Septal Defect",
        "Boxer Myocardial Disease",
        "Congenital Pericardial Malformations",
        "Constrictive Pericardial Disease",
        "Degenerative Valve Disease",
        "Infective Endocarditis",
        "Ischemic Heart Disease",
        "Mitral Valve Dysplasia",
        "Pulmonic Stenosis",
        "Tricuspid Valve Dysplasia",
        "Ventricular Septal Defect"
      ],
      "severity": "low",
      "description": "gets tired quickly"
    },
    "Pale Tongue/White Tongue And Pale Gums": {
      "diseases": [
        "Atrial Septal Defect",
        "Dilated Cardiomyopathy",
        "Tetralogy Of Fallot"
      ],
      "severity": "low"
    },
    "Changes In Behavior": {
      "diseases": [
        "Boxer Myocardial Disease",
        "Cardiac Tumors",
        "Congenital Pericardial Malformations",
        "Degenerative Valve Disease",
        "Infective Endocarditis",
        "Ischemic Heart Disease"
      ],
      "severity": "low",
      "description": "less playful or depressed; reluctance to exercise/walk; restless - especially at night; hiding"
    },
    "Weakness": {
      "diseases": [
        "Cardiac Tumors",
        "Constrictive Pericardial Disease",
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Infective Endocarditis",
        "Ischemic Heart Disease",
        "Mitral Valve Dysplasia",
        "Patent Ductus Arteriosus",
        "Subaortic Stenosis",
        "Tetralogy Of Fallot",
        "Tricuspid Valve Dysplasia"
      ],
      "severity": "low"
    },
    "Weight Loss": {
      "diseases": [
        "Degenerative Valve Disease"
      ],
      "severity": "low"
    },
    "Loss Of Appetite": {
      "diseases": [
        "Degenerative Valve Disease",
        "Dilated Cardiomyopathy",
        "Infective Endocarditis",
        "Ischemic Heart Disease"
      ],
      "severity": "low"
    },
    "Difficulty Eating Solid Food": {
      "diseases": [
        "Persistent Right Aortic Arch"
      ],
      "severity": "low"
    }
  },
  "groups": {}

}
