const dogScreening = require('./data');

function unique(value, index, self) { 
    return self.indexOf(value) === index;
}

function isRiskyBreed(breed) {
  if ((typeof breed !== 'undefined') && 
      (typeof dogScreening['breeds'][breed] !== 'undefined') &&
      (typeof dogScreening['breeds'][breed].diseases !== 'undefined')) {
    return true;
  }


  return false;
}

function diseaseHasGenderPrevalance(disease, isMale, isFemale, breed) {
  if ((isMale && dogScreening['diseases'][disease].males && dogScreening['diseases'][disease].males.includes(breed)) ||
      (isFemale && dogScreening['diseases'][disease].females && dogScreening['diseases'][disease].females.includes(breed))) {
    return true;    
  }
  return false;
}

function getDiseasesByBreed(breed) {
  var results = [];
  var data = dogScreening['breeds'][breed];
  if (typeof data !== 'undefined') {
    var diseasesByBreed = data.diseases;
    if (typeof diseasesByBreed !== 'undefined') {
      for (var i = 0; i < diseasesByBreed.length; i++) {
        var disease = diseasesByBreed[i];
        results.push(disease);
      }
    }
  }
  return results;
}

function getResults(params) {
  var results = {
    diseasesByBreed: [],
    diseasesByBreedAndSymptoms: [],
    diseasesBySymptomsRest: [],
    diseasesByGenderPrevalance: [],
    diseasesLinks: {}
  };

  var i, j;
  var numOfSymptoms = params.symptoms.length;
  var numOfHighRiskSymptoms = 0;

  var diseasesByBreed = getDiseasesByBreed(params.breed);
  var diseasesByBreedII = getDiseasesByBreed(params.breedII);
  results.diseasesByBreed = diseasesByBreed.concat(diseasesByBreedII).filter(unique);

  for (j = 0; j < params.symptoms.length; j++) {
    var symptom = params.symptoms[j];
    if (dogScreening['symptoms'][symptom].severity == "high") {
      numOfHighRiskSymptoms++;
    }
    var diseasesBySymptoms = dogScreening['symptoms'][symptom].diseases;
    for (i = 0; i < diseasesBySymptoms.length; i++) {
      var disease = diseasesBySymptoms[i];
      if (results.diseasesByBreed.includes(disease)) {
        if (!results.diseasesByBreedAndSymptoms.includes(disease)) {
          results.diseasesByBreedAndSymptoms.push(disease);
        }
      }
      else {
        if (!results.diseasesBySymptomsRest.includes(disease)) {
          results.diseasesBySymptomsRest.push(disease);
        }
      }
    }
  }

  var allDiseases = results.diseasesBySymptomsRest.concat(results.diseasesByBreed);
  for (i = 0; i < allDiseases.length; i++) {
    var disease = allDiseases[i];

    if (diseaseHasGenderPrevalance(disease, params.isMale, params.isFemale, params.breed) ||
        diseaseHasGenderPrevalance(disease, params.isMale, params.isFemale, params.breedII)) {
      results.diseasesByGenderPrevalance.push(disease);
    }
    
    results.diseasesLinks[disease] = dogScreening['diseases'][disease].link;
  }

  var riskyBreed = isRiskyBreed(params.breed) || isRiskyBreed(params.reedII);


  var severity = 'green';
  if ((params.age >= 9 && params.age <= 12) || (riskyBreed && numOfSymptoms === 1) || (!riskyBreed && numOfSymptoms == 2)) {
    severity = 'yellow'
  }
  if ((params.age > 12) || (numOfHighRiskSymptoms > 0) || (riskyBreed && numOfSymptoms > 1) || (!riskyBreed && numOfSymptoms > 2)) {
    severity = 'red';
  }

  return {
    severity: severity,
    data: results
  }
}

module.exports = getResults;