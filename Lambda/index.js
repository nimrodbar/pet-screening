const getResults = require('./getResults')
const db = require('./db')

exports.handler = async (event) => {
  console.log(event)
  const request = JSON.parse(event.body)
  const response = getResults(request)
  await db.put(request, response, { country: event.headers['CloudFront-Viewer-Country'] })
  return {
    "statusCode": 200,
    "headers": {
      "Access-Control-Allow-Origin" : "*",
      "Content-Type": "application/json"
    },
    "body": JSON.stringify(response)
  }
};
